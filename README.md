# EEE3088F Group Project

## Welcome to group 28's EEE3088F project, the Top HAT!

<img src="https://gitlab.com/mikhailrussell/eee3088f-group-project/-/raw/main/Documentation/Pictures/Top%20HAT.jpeg" alt="The Top Hat" width="400"/>

# Group members

Sarah Tallack (@sarahtallack)

Stefan Friedrich (@StefanF2)

Mikhail Russell (@mikhailrussell)

# Purpose of the project

The goal of this project was to design a piece of Hardware Attached on Top (HAT) for the STM32F051 Discovery Kit. The HAT needed to meet the following criteria:
- It had to have a 5V lithium-ion battery, which could power the HAT
- It had to have Under Voltage Lockout Protection (UVLO) to avoid battery over-discharge
- The battery had to be rechargeable via a battery charging circuit
- The voltage of the battery needed to be stepped down to 3.3V for the microcontroller and the rest of the circuit
- The board had to have one ADC sensor
- The board had to have one Digital Sensor, using the I2C protocol
- The data from the sensors had to be stored in an on-board EEPROM chip
- If a USB Micro-B cable was plugged into the HAT, the attached computer had to receive the data stored in the EEPROM

# Submodules

The project task was broken down into three submodules and every group member was allocated to work on one submodule, as follows:
- :zap: Power - Stefan Friedrich
- :eyes: Sensing - Mikhail Russell
- :handshake: Microcontroller Interfacing - Sarah Tallack 

# Build status

The Top HAT v1.0 has just finished its testing phase. Notes were taken and lessons were learnt. The designs for the Top HAT v1.1 are currently under construction. :construction_worker: 

# Hardware needed

To make full use of the Top HAT, the following is needed:
- The Top HAT
- A STM32F051 Discovery Kit microcontroller
- A laptop or desktop with a USB A port
- A USB A to USB Mini-B cable
- A USB A to USB Micro-B cable

# Software and tools needed

The following tools and software are needed:
- [STMCube IDE] (recommended) or any other C-based IDE capable of flashing code to the STM32F051 microcontroller
- [PuTTY]

# Getting Started

## Flashing code to the microcontroller

### 1. Setting up the hardware

Plug the USB A connector into your laptop and connect the USB Mini-B connector end to the STM32F051 microcontroller. Make sure that your laptop or desktop is on, and that the LD1 and LD2 LEDs of your STM32F051 microcontroller are on or flashing.

### 2. Setting up the software

Download the microcontroller code [from here](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Scripts/workspace_1.9.0/USB%20demo). Open the code using STMCube IDE (or any other IDE of your choice). Flash the code to the microcontroller using the IDE.

## Using the Top HAT

### 1. Setting up the hardware

Make sure that the lithium-ion battery is charged and functional. Close all connectors on the HAT (except for J9 and J10) using jumpers. Slot the STM32F051 microcontroller into the corresponding pin holes on the Top HAT. Make sure that the USB port on the microcontroller and the USB port on the HAT both point in the same direction when doing so. Plug the USB Micro-B connector into the port on the Top HAT and the USB A connector end into the laptop or desktop.

### 2. Setting up the software

Open up the PuTTY window on your computer. Select the type of serial connection being used (e.g. COM3, COM4, etc). Check to see if a window, showing the output of the sensors, is opened on your computer.

## Example Program

To ensure that the hardware and software work correctly, a trial program may be run. Configure the hardware and software in the same manner as described in the "Getting Started" section. The code for this test program may be found [here](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Scripts/workspace_1.9.0/EEPROM%20and%20UART). If everything works as expected, the window opened by PuTTY should display a hello message and LD4 on the STM32F051 microcontroller should start to flash.

# Features

## Sensors

The sensor circuit consists of the LTR-553ALS-01 as the digital sensor and a voltage divider across the battery to measure the voltage across it.

The LTR-553ALS-01 is an ambient light and proximity sensor. It uses I2C to communicate with the STM32F051 microcontroller.

## USB protocol

The circuit makes use of an FTDI chip which allows the STM32F051 microcontroller to communicate with your computer using the UART communication protocol. The microcontroller takes the sensor data stored in the EEPROM and transmits it to the computer. The data from the sensor can be received on the computer using [PuTTY].

## Power

On the PCB there is space for a 5V lithium-ion battery holder to be soldered on. The battery voltage is stepped down to 3.3V for the rest of the circuit to operate off. The battery can also be charged via a battery charging IC, which operates off of the 5V line connection from the Top HAT's USB port.

# License

This project uses the CC BY-SA license type. If you adapt this work in any way, please give credit to the creator and make sure that your adaptation also uses the same license.

# Getting involved

Should you wish to contribute to this project, please have a look at the [Contributing.md](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/CONTRIBUTING.md) file first.

# Where to find stuff

To begin using the Top HAT, please refer back to the "Getting Started" guide. For answers to more detailed and specific questions, please refer to the "How To" guide.

In the PCB folder, the group is currently working on V1.1 of the board. The schematics are up to date but make sure to open MAIN_3, regarding the PCB.
This folder also has the schematics for each of the submodules. 
When opening the project, please be reminded to add the libraries from the Project_Footprints folder and the symbol libraries from PCB_Library_From_Template.kicad_sym. Both can be found in the PCB folder.

The datasheets for all parts and components used can be found as PDFs in the project's [Documentation](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Documentation) directory.

The final BOM, Position files and Gerbers can be found [here](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/PCB/EEE3088F_Project_PCB/jlcpcb).

The Code Reference document is found within the [Scripts](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Scripts) directory and gives information on the code used and where to find it.

[STMCube IDE]: https://www.st.com/en/development-tools/stm32cubeide.html
[PuTTY]: https://www.putty.org/

