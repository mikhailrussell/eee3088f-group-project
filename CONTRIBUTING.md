# Contributing to the TOP hat project

## Getting started

If you would like to start contributing to the project, simply submit a pull request on Gitlab. Once you have been approved branch off of the main branch and begin contributing, using the following guide.

## Code of Conduct

This project is governed by the GitLab Community Code of Conduct, found [here](https://about.gitlab.com/community/contribute/code-of-conduct/).

## Reporting bugs: 

If you find anything wrong with the schematics or the microcontroller code, please raise the issue on Gitlab. This will enable respective team members to address the issue. If you have a possible solution to the bug, feel free to suggest it in the report.

## Commiting changes

To add your changes to the repository, submit a push request so that your changes can be reviewed.

## Software required:

In order to easily configure the pins for the discovery board, and contribute to the code, the [STMCube IDE](https://www.st.com/en/development-tools/stm32cubeide.html) is recommended.

To get data from the board, once it has been powered and plugged into a computer, [Putty](https://www.putty.org/) is recommended.

## Style guides:

### Git commit messages
- Ensure that all of your commit are accompanied by a descriptive messages
- Use the present tense in your messages

### Schematic changes
- Ensure you change the version of the schematic when changes are made
- Label any circuits added to the Project
- Give a brief description of what the circuit is meant to do, and feel free to add yourself as an author

### STM32 Discovery Board code changes
- For any new functions, give a brief description in the code of what the function is used for, what its parameters are, and what value it returns
- Ensure you use proper indentation in your code 
- Only add code in the USER CODE sections (indicated by comments), otherwise the code won't be saved if the code is regenerated. 
- Use the USER CODE 4 section to add additional functions
- When naming variables, ensure it is clear what the variable is storing/used for

## Schematics and PCB:

First, check out the schematics in the PCB/EEE3088F_Project_PCB folder (The project you should click on is EEE3088F_Project_PCB.project ). There are 4 schematics, 3 of which are submodules of the main circuit. These are Main, Sensor, Power and Microcontroller Interfacing files.
All the components are in the BOM.

## LINKS: 

BOM: https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/PCB/EEE3088F_Project_PCB/jlcpcb/assembly/Group_28_BOM.csv

Main Kicad Project: https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/PCB/EEE3088F_Project_PCB/EEE3088F_Project_PCB.kicad_pro

If you want the correct netlist, it is netlist V4.

Gerbers, BOM and Pos files in format for JLCPCB: https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/PCB/EEE3088F_Project_PCB/jlcpcb

The final code used is the in the Scripts\workspace_1.9.0 and is called USB demo. Run the project in STMCube IDE and upload the code to your STMF0 Discovery Board.

