/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stm32f0xx_hal.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define LTR_ADDR 0x23<<1
volatile uint16_t ALS_CH0_ADC_Data;
volatile uint16_t ALS_CH1_ADC_Data;
volatile uint8_t ALS_Data;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
char str[60] = {0}; 						//Useful buffer for printing to UART
uint8_t I2CReturn = 0; 						//Status var to indicate if HAL_I2C operation has succeeded (1) or failed (0);
uint16_t EEPROM_DEVICE_ADDR = 0x50 << 1; 	//Address of EEPROM device on I2C bus

//Setup variables for reading and writing
//    uint16_t madd = 0x00; 						//Memory address variable containing a starting memory address for a location of memory in the EEPROM
//    uint8_t Data = 0x10;						//Data variable containing starting value to write to memory, could be any 8bit value
//    uint8_t *sData = &Data; 					//Pointer to sending Data variable
    uint8_t Result = 0x00; 						//Variable to stored value read back from memory in
    uint8_t *rData = &Result; 					//Pointer to result data variable

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
void debugPrintln(UART_HandleTypeDef *uart_handle, char _out[]);
void EEPROM_write(uint8_t Data, uint8_t mem_Address);
void EEPROM_read(uint8_t mem_Address);
void init_LTR(void);
void LTR_write_register(uint8_t register_pointer, uint16_t register_value);
void read_LTR_ALS(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//General purpose function to send a char array over the UART and to automatically send a new line after it
void debugPrintln(UART_HandleTypeDef *uart_handle, char _out[]) {
	HAL_UART_Transmit(uart_handle, (uint8_t *) _out, strlen(_out), 60);
	char newline[2] = "\r\n";
	HAL_UART_Transmit(uart_handle, (uint8_t *) newline, 2, 10);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
    uint8_t i, j, Loop = 0; 					//Loop counters



    HAL_Delay(2000); //Delay start for 500 ms, so that Putty can be started once the board is plugged in

    //Say hello over UART
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
    debugPrintln(&huart1, "Hello, this is STMF0 Discovery board: ");

    uint8_t madd = 0x00;

    init_LTR();

    uint8_t k = 0, I2Creturn;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  read_LTR_ALS();
	  //Print and increment a loop counter for visual tracking/debugging purposes only
	  memset(str, 0, sizeof(str)); //Reset str to zeros
	  sprintf(str, "\rLoop count %d\n", Loop);//Format string to include the loop counter variable
	  debugPrintln(&huart1, str);
	  Loop = Loop + 1;

	  // Write data from sensor to EEPROM at address madd every 100 ms for 5 values
	  for (i = 0; i < 5; i++) {
		  EEPROM_write(ALS_Data, madd);
		  read_LTR_ALS();
		  madd++;
		  HAL_Delay(1000);
	  }


	  /* Check if USB is plugged in, if PA4 goes high. If USB is detected, send data from EEPROM to computer */
	  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4)) {
		  //turn LED on
		  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
		  debugPrintln(&huart1, "USB detected!");

		  //read from EEPROM and print result through UART


		  madd = madd - 5;
		  for (j = 0; j < 5; j++) {
			  EEPROM_read(madd);
			  madd++;
		  }

		  /*
		  int n=0;
		  while (n < madd){
			  memset(str, 0, sizeof(str)); //Reset str to zeros
			 sprintf(str, "\rLoop count %d\n", madd);//Format string to include the loop counter variable
			 debugPrintln(&huart1, str);
			  EEPROM_read(madd);
			  HAL_Delay(100);

		  }

		  Loop = 0;*/
	  }
	  else {
		  //turn LED off
	  	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
	  	  debugPrintln(&huart1, "No USB detected!");

	  }

	  HAL_Delay(2000);

	  //read_LTR_ALS();

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x20303E5D;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void EEPROM_write(uint8_t Data, uint8_t mem_Address)
{
	/* Function to write 8 bits of data to the EEPROM at the selected memory address */
	uint8_t *sData = &Data; 							// Pointer to sending Data variable
	memset(str, 0, sizeof(str));						// Reset str variable
	sprintf(str, "Writing 0x%X to EEPROM address 0x%X", Data, mem_Address);
	debugPrintln(&huart1, str);
	I2CReturn = HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEVICE_ADDR, mem_Address, 2, sData, 1, HAL_MAX_DELAY);
	if (I2CReturn != HAL_OK) {
		debugPrintln(&huart1, "Write to address FAILED");
	}
}

void EEPROM_read(uint8_t mem_Address)
{
	/* Function to read from EEPROM at selected memory address and then print the results */
	memset(str, 0, sizeof(str));
	sprintf(str, "Reading from EEPROM address 0x%X ", mem_Address);
	debugPrintln(&huart1, str);
	I2CReturn = HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEVICE_ADDR, mem_Address, 2, rData, 1, HAL_MAX_DELAY);

	if (I2CReturn != HAL_OK) {
		debugPrintln(&huart1, "Read from address FAILED");
	}

	//PRINT READ VALUE
	memset(str, 0, sizeof(str));
	sprintf(str, "Received data: 0x%X \n", Result);
	debugPrintln(&huart1, str);
}

//  MMikhails code

void init_LTR(void)
{
	debugPrintln(&huart1, "Wait!");
	HAL_Delay(1000);

	uint8_t Register_Addr;
	int8_t Command;

	debugPrintln(&huart1, "Enabling ALS_Contr Register");

	Register_Addr = 0x80; // ALS_CONTR register
	Command = 0x01; // For Gain X1
					// For Gain X2, Command = 0x05
					// For Gain X4, Command = 0x09
					// For Gain X8, Command = 0x0D
					// For Gain X48, Command = 0x19
					// For Gain X96, Command = 0x1D
	LTR_write_register(Register_Addr, Command);

	// Enable PS

	debugPrintln(&huart1, "Enabling PS_Contr Register");
	Register_Addr = 0x81; // PS_CONTR register
	Command = 0x03;
	LTR_write_register(Register_Addr, Command);

	// The PS LED Registers define the LED pulse modulation frequency, duty cycle and peak current.
	// Default setting is 0x7F (60kHz, 100%, 100mA).
	// Set LED Pulse Freq 30kHz (duty cycle 100%, peak curr 100mA)
	Register_Addr = 0x82; // PS_LED register
	Command = 0x3F; // Custom cycle
					// For Pulse Freq = 40kHz, (100%, 100mA), Command = 0x3F
					// For Pulse Freq = 50kHz, (100%, 100mA), Command = 0x5F
					// For Pulse Freq = 60kHz, (100%, 100mA), Command = 0x7F
					// For Pulse Freq = 70kHz, (100%, 100mA), Command = 0x9F
					// For Pulse Freq = 80kHz, (100%, 100mA), Command = 0xBF
					// For Pulse Freq = 90kHz, (100%, 100mA), Command = 0xDF
					// For Pulse Freq = 100kHz, (100%, 100mA), Command = 0xFF

	LTR_write_register(Register_Addr, Command);

	// Set LED Duty Cycle 25% (pulse freq 60kHz, peak curr 100mA)
	Register_Addr = 0x82; // PS_LED register
	Command = 0x67; // Duty Cycle = 25%, (pulse freq 60kHz, peak curr 100mA)
					// For Duty Cycle = 50%, (60kHz, 100mA), Command = 0x6F
					// For Duty Cycle = 75%, (60kHz, 100mA), Command = 0x77
					// For Duty Cycle = 100%, (60kHz, 100mA), Command = 0x7F
	LTR_write_register(Register_Addr, Command);

	// Set LED Peak Current 5mA (pulse freq 60kHz, duty cycle 100%)
	Register_Addr = 0x82; // PS_LED register
	Command = 0x78; // Peak Current = 5mA, (pulse freq 60kHz, duty cyc 100%)
	// For Peak Current = 10mA, (60kHz, 100%), Command = 0x79
	// For Peak Current = 20mA, (60kHz, 100%), Command = 0x7A
	// For Peak Current = 50mA, (60kHz, 100%), Command = 0x7B
	LTR_write_register(Register_Addr, Command);
}

void LTR_write_register(uint8_t register_pointer, uint16_t register_value)
{
	//uint8_t data[2];
	//data[0] = register_pointer;
	//data[1] = register_value;

	uint8_t *sregister_value = &register_value;

    //HAL_I2C_Master_Transmit(&hi2c1, LTR_ADDR, data, 2, 100);  // data is the start pointer of our array
	I2CReturn = HAL_I2C_Mem_Write(&hi2c2, LTR_ADDR, register_pointer, 1, sregister_value, 1, 100);
		if (I2CReturn != HAL_OK) {
			debugPrintln(&huart1, "LTR Write to address FAILED");
		}
}

void read_LTR_ALS(void){

	// The ALS Data Registers contain the ADC output data for the respective channel.
	// These registers should be read as a group, with the lower address being read first.
	// Read back ALS_DATA_CH1

	uint8_t Data0 = 0x10;
	uint8_t *sData0 = &Data0;
	uint8_t Data1 = 0x10;
	uint8_t *sData1 = &Data1;
	uint8_t Data2 = 0x10;
	uint8_t *sData2 = &Data2;
	uint8_t Data3 = 0x10;
	uint8_t *sData3 = &Data3;
	uint8_t Register_Addr;


	Register_Addr = 0x88; // ALS_DATA_CH1 low byte address
	I2CReturn = HAL_I2C_Mem_Read(&hi2c2, LTR_ADDR, Register_Addr, 1, sData0, 1, 100);
	if (I2CReturn != HAL_OK) {
				debugPrintln(&huart1, "LTR read ALS1 Failed");
			}

	ALS_Data = Data0;

	//LTR_read_register(Register_Addr, Data0);
	Register_Addr = 0x89; // ALS_DATA_CH1 high byte address
	I2CReturn = HAL_I2C_Mem_Read(&hi2c2, LTR_ADDR, Register_Addr, 1, sData1, 1, 100);
	if (I2CReturn != HAL_OK) {
					debugPrintln(&huart1, "LTR read ALS1 Failed");
				}

	//LTR_read_register(Register_Addr, Data1[1]);

	// Read back ALS_DATA_CH0
	Register_Addr = 0x8A; // ALS_DATA_CH0 low byte address
	I2CReturn = HAL_I2C_Mem_Read(&hi2c2, LTR_ADDR, Register_Addr, 1, sData2, 1, 100);
	if (I2CReturn != HAL_OK) {
						debugPrintln(&huart1, "LTR read ALS1 Failed");
					}

	//LTR_read_register(Register_Addr, Data2);
	Register_Addr = 0x8B; // ALS_DATA_CH0 high byte address
	HAL_I2C_Mem_Read(&hi2c2, LTR_ADDR, Register_Addr, 1, sData3, 1, 100);
	//LTR_read_register(Register_Addr, Data3);
	ALS_CH1_ADC_Data = (Data1 << 8) | Data0; // Combining lower and upper bytes to give 16-bit Ch1 data
	ALS_CH0_ADC_Data = (Data3 << 8) | Data2;// Combining lower and upper bytes to give 16-bit Ch0 data

	//PRINT READ VALUE
	memset(str, 0, sizeof(str));
	sprintf(str, "Received data from ALSCH1: 0x%X \n", ALS_CH1_ADC_Data);
	debugPrintln(&huart1, str);

	memset(str, 0, sizeof(str));
	sprintf(str, "Received data from ALSCH0: 0x%X \n", ALS_CH0_ADC_Data);
	debugPrintln(&huart1, str);

	ALS_Data = Data0;



}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
