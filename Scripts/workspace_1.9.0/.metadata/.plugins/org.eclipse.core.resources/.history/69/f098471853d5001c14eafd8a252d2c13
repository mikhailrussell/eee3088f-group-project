/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

#define LTR_ADDR 0x23 << 1
 uint16_t ALS_CH0_ADC_Data;
 uint16_t ALS_CH1_ADC_Data;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim14;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM14_Init(void);
/* USER CODE BEGIN PFP */
void init_LTR(void);
void read_LTR_ALS(void);
void LTR_write_register(uint8_t register_pointer, uint16_t register_value);
void LTR_read_register(uint8_t register_pointer, uint8_t* register_buffer);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  init_LTR();

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	HAL_Delay(1000);
	//read_LTR_ALS();
	HAL_Delay(1000);
	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 0;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 65535;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void init_LTR(void)
{
	uint8_t Register_Addr;
	int8_t Command;

	Register_Addr = 0x80; // ALS_CONTR register
	Command = 0x01; // For Gain X1
					// For Gain X2, Command = 0x05
					// For Gain X4, Command = 0x09
					// For Gain X8, Command = 0x0D
					// For Gain X48, Command = 0x19
					// For Gain X96, Command = 0x1D
	LTR_write_register(Register_Addr, Command);

	// Enable PS
	Register_Addr = 0x81; // PS_CONTR register
	Command = 0x03;
	LTR_write_register(Register_Addr, Command);

	// The PS LED Registers define the LED pulse modulation frequency, duty cycle and peak current.
	// Default setting is 0x7F (60kHz, 100%, 100mA).
	// Set LED Pulse Freq 30kHz (duty cycle 100%, peak curr 100mA)
	Register_Addr = 0x82; // PS_LED register
	Command = 01110000; // Custom cycle
					// For Pulse Freq = 40kHz, (100%, 100mA), Command = 0x3F
					// For Pulse Freq = 50kHz, (100%, 100mA), Command = 0x5F
					// For Pulse Freq = 60kHz, (100%, 100mA), Command = 0x7F
					// For Pulse Freq = 70kHz, (100%, 100mA), Command = 0x9F
					// For Pulse Freq = 80kHz, (100%, 100mA), Command = 0xBF
					// For Pulse Freq = 90kHz, (100%, 100mA), Command = 0xDF
					// For Pulse Freq = 100kHz, (100%, 100mA), Command = 0xFF

	LTR_write_register(Register_Addr, Command);

	// Set LED Duty Cycle 25% (pulse freq 60kHz, peak curr 100mA)

	Register_Addr = 0x82; // PS_LED register
	Command = 0x67; // Duty Cycle = 25%, (pulse freq 60kHz, peak curr 100mA)
					// For Duty Cycle = 50%, (60kHz, 100mA), Command = 0x6F
					// For Duty Cycle = 75%, (60kHz, 100mA), Command = 0x77
					// For Duty Cycle = 100%, (60kHz, 100mA), Command = 0x7F
	LTR_write_register(Register_Addr, Command);
}

void read_LTR_ALS(void){

	// The ALS Data Registers contain the ADC output data for the respective channel.
	// These registers should be read as a group, with the lower address being read first.
	// Read back ALS_DATA_CH1

	uint8_t Data0 = 0x10;
	uint8_t *sData0 = &Data0;
	uint8_t Data1 = 0x10;
	uint8_t *sData1 = &Data1;
	uint8_t Data2 = 0x10;
	uint8_t *sData2 = &Data2;
	uint8_t Data3 = 0x10;
	uint8_t *sData3 = &Data3;
	uint8_t Register_Addr;

	Register_Addr = 0x88; // ALS_DATA_CH1 low byte address
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, 1, sData0, 1, HAL_MAX_DELAY);


	//LTR_read_register(Register_Addr, Data0);
	Register_Addr = 0x89; // ALS_DATA_CH1 high byte address
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, 1, sData1, 1, HAL_MAX_DELAY);

	//LTR_read_register(Register_Addr, Data1[1]);

	// Read back ALS_DATA_CH0
	Register_Addr = 0x8A; // ALS_DATA_CH0 low byte address
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, 1, sData2, 1, HAL_MAX_DELAY);

	//LTR_read_register(Register_Addr, Data2);
	Register_Addr = 0x8B; // ALS_DATA_CH0 high byte address
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, 1, sData3, 1, HAL_MAX_DELAY);
	//LTR_read_register(Register_Addr, Data3);
	ALS_CH1_ADC_Data = (Data1 << 8) | Data0; // Combining lower and upper bytes to give 16-bit Ch1 data
	ALS_CH0_ADC_Data = (Data3 << 8) | Data2;// Combining lower and upper bytes to give 16-bit Ch0 data
}

void LTR_write_register(uint8_t register_pointer, uint16_t register_value)
{
	//uint8_t data[2];
	//data[0] = register_pointer;
	//data[1] = register_value;

    //HAL_I2C_Master_Transmit(&hi2c1, LTR_ADDR, data, 2, 100);  // data is the start pointer of our array
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, register_pointer, 1, register_value, 1, HAL_MAX_DELAY);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
