void init_LTR(void)
{
	uint8_t Register_Addr;
	uint16_t Command;

	Register_Addr = 0x80; // ALS_CONTR register
	Command = 0x01; // For Gain X1
					// For Gain X2, Command = 0x05
					// For Gain X4, Command = 0x09
					// For Gain X8, Command = 0x0D
					// For Gain X48, Command = 0x19
					// For Gain X96, Command = 0x1D
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), Command, sizeof(Command), 50);

	// Enable PS
	Register_Addr = 0x81; // PS_CONTR register
	Command = 0x03;
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), Command , sizeof(Command), 50);


	// The PS LED Registers define the LED pulse modulation frequency, duty cycle and peak current.
	// Default setting is 0x7F (60kHz, 100%, 100mA).
	// Set LED Pulse Freq 30kHz (duty cycle 100%, peak curr 100mA)
	Register_Addr = 0x82; // PS_LED register
	Command = 01110000; // Custom cycle
					// For Pulse Freq = 40kHz, (100%, 100mA), Command = 0x3F
					// For Pulse Freq = 50kHz, (100%, 100mA), Command = 0x5F
					// For Pulse Freq = 60kHz, (100%, 100mA), Command = 0x7F
					// For Pulse Freq = 70kHz, (100%, 100mA), Command = 0x9F
					// For Pulse Freq = 80kHz, (100%, 100mA), Command = 0xBF
					// For Pulse Freq = 90kHz, (100%, 100mA), Command = 0xDF
					// For Pulse Freq = 100kHz, (100%, 100mA), Command = 0xFF

	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), Command, sizeof(Command), 50);

	// Set LED Duty Cycle 25% (pulse freq 60kHz, peak curr 100mA)

	Register_Addr = 0x82; // PS_LED register
	Command = 0x67; // Duty Cycle = 25%, (pulse freq 60kHz, peak curr 100mA)
					// For Duty Cycle = 50%, (60kHz, 100mA), Command = 0x6F
					// For Duty Cycle = 75%, (60kHz, 100mA), Command = 0x77
					// For Duty Cycle = 100%, (60kHz, 100mA), Command = 0x7F
	HAL_I2C_Mem_Write(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), Command, sizeof(Command), 50);
}

void read_LDR_ALS(void){

	// The ALS Data Registers contain the ADC output data for the respective channel.
	// These registers should be read as a group, with the lower address being read first.
	// Read back ALS_DATA_CH1

	uint8_t Data0;
	uint8_t Data1;
	uint8_t Data2;
	uint8_t Data3;
	uint8_t Register_Addr;
	uint16_t Command;

	Register_Addr = 0x88; // ALS_DATA_CH1 low byte address
	HAL_I2C_Mem_Read(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), uint8_t * Data0, sizeof(Data0), 50);
	Register_Addr = 0x89; // ALS_DATA_CH1 high byte address
	HAL_I2C_Mem_Read(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), uint8_t * Data1, sizeof(Data1), 50);

	// Read back ALS_DATA_CH0
	Register_Addr = 0x8A; // ALS_DATA_CH0 low byte address
	HAL_I2C_Mem_Read(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), uint8_t * Data2, sizeof(Data3), 50);
	Register_Addr = 0x8B; // ALS_DATA_CH0 high byte address
	HAL_I2C_Mem_Read(&hi2c1, LTR_ADDR, Register_Addr, sizeof(Register_Addr), uint8_t * Data3, sizeof(Data3), 50);

	ALS_CH1_ADC_Data = (Data1 << 8) | Data0; // Combining lower and upper bytes to give 16-bit Ch1 data
	ALS_CH0_ADC_Data = (Data3 << 8) | Data2;// Combining lower and upper bytes to give 16-bit Ch0 data
}