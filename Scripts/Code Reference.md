## Code Reference
<span style="text-decoration:underline;">Introduction</span>

The STMCubeIDE project that this reference sheet is based off of can be found in Scripts/workspace_1.9.0/USB Demo ([https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Scripts/workspace_1.9.0/USB%20demo](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/tree/main/Scripts/workspace_1.9.0/USB%20demo)) and should be opened using STMCubeIDE. All the functions are contained in `main.c` ([https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/Scripts/workspace_1.9.0/USB%20demo/Core/Src/main.c](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/Scripts/workspace_1.9.0/USB%20demo/Core/Src/main.c)). The code revolves around updating the ALS_CH0_ADC_Data, ALS_CH1_ADC_Data_ _and ALS_Data_ _variables and writing this to the EEPROM.

<span style="text-decoration:underline;">Includes</span>

#include "string.h"

Used to print out strings during EEPROM communication

#include "stm32f0xx_hal.h"

Contains STM’s HAL library. Used for I2C and UART communication with peripherals.

#include "stdio.h"

Standard Input Output Library for C.

<span style="text-decoration:underline;">Initialisation</span>

Most of the initialisation of the STM is handled by STMCubeIDE. The initialisation functions are summarised in the following table:


<table>
  <tr>
   <td><span style="text-decoration:underline;">Function</span>
   </td>
   <td><span style="text-decoration:underline;">Description</span>
   </td>
  </tr>
  <tr>
   <td><code>SystemClock_Config(void)</code>
   </td>
   <td>Initialises the RCC Oscillators
   </td>
  </tr>
  <tr>
   <td><code>MX_GPIO_Init(void)</code>
   </td>
   <td>Initialises the GPIO pins on the STM Board 
   </td>
  </tr>
  <tr>
   <td><code>MX_I2C1_Init(void)</code>
   </td>
   <td>Initialises I2C communication for communicating with the EEPROM
   </td>
  </tr>
  <tr>
   <td><code>MX_I2C2_Init(void)</code>
   </td>
   <td>Initialises I2C communication for communicating with the LTR sensor
   </td>
  </tr>
  <tr>
   <td><code>MX_USART1_UART_Init(void)</code>
   </td>
   <td>Initialises I2C communication for FTDI chip for USB communication
   </td>
  </tr>
  <tr>
   <td><code>init_LTR(void)</code>
   </td>
   <td>Initialises the LTR Sensor with default parameters (gain = X1, LED Pulse Frequency of 30kHz, duty cycle 100%, peak current 100mA)
   </td>
  </tr>
</table>


<span style="text-decoration:underline;">Reading from EEPROM</span>

Two functions have been created to simplify communication with EEPROM. The standard EEPROM address is 0x50.


```
void EEPROM_write(uint8_t Data, uint8_t mem_Address)
```


Description:

This function allows the user to store data into the EEPROM at a specific address.



* `uint8_t Data`: The 8 bit variable that needs to be stored in the EEPROM.
* `uint8_t mem_Address`: The address that it Data will be stored in.


```
void EEPROM_read(uint8_t mem_Address)
```


Description:

Reads the data at a specific memory address and prints the result using UART.



* uint8_t mem_Address: Address which user wants to print.

<span style="text-decoration:underline;">Updating Sensor Data</span>

Two functions have been made to simplify communication with the LTR sensor:


```
void LTR_write_register(uint8_t register_pointer, uint16_t register_value)
```


Description:

This function is mainly used to initialise the sensor on start up by writing commands to specific registers. The user can change initialisation conditions by determining the register address and command value from the LTR-553ALS-01 documentation sheet ([https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/Documentation/Lite-On-LTR-553ALS-01_C492375.pdf](https://gitlab.com/mikhailrussell/eee3088f-group-project/-/blob/main/Documentation/Lite-On-LTR-553ALS-01_C492375.pdf)).



* `uint8_t register_pointer: `The address where the user wants to write to.
* `uint16_t register_value: `The register value that needs to be written.


```
void read_LTR_ALS(void)
```


Description:

Running this function update<code>s ALS_CH0_ADC_Data, ALS_CH1_ADC_Data<em> </em></code>and <code>ALS_Data<em> </em></code>variables. These contain the LUX data readings. No variable needs to be imputed as the function handles the communication.

Main Function

The main() function starts off by running all the initialization functions. A loop is then ran 5 times, updating the ALS registers using read_LTR_ALS() and writing this data to the EEPROM using EEPROM_write(). The STM then checks if the USBis plugged into HAT. If it is, it reads the last 5 addresses on the EEPROM using EEPROM_read() and outputs it using UART and the FTDI chip. Otherwise if the USB is not detected, the loop is run again.

